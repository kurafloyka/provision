package test;

public class ManagerAllReadings {

    private IreadJsonFiles ireadJsonFiles;

    public ManagerAllReadings(IreadJsonFiles ireadJsonFiles) {
        this.ireadJsonFiles = ireadJsonFiles;
    }

    public void readJson() {

        ireadJsonFiles.readJson();
    }
}
