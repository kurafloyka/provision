package test;

import Models.last24.Last24;
import Models.load.ExtReference;
import Models.logslist.LogList;
import org.junit.Test;



public class ReadJson {
    public ManagerAllReadings managerAllReading;


    @Test
    public void readLast24() {

        managerAllReading = new ManagerAllReadings
                (new Last24("/src/main/resources/last24.json"));
        managerAllReading.readJson();
    }

    @Test
    public void readExtReference() {
        managerAllReading = new ManagerAllReadings
                (new ExtReference("/src/main/resources/extReference.json"));
        managerAllReading.readJson();

    }

    @Test
    public void readProvisionLog() {
        managerAllReading = new ManagerAllReadings
                (new LogList("/src/main/resources/logslist.json"));
        managerAllReading.readJson();


    }
}

