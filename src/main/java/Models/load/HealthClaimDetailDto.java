
package Models.load;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HealthClaimDetailDto {

    @SerializedName("claimId")
    @Expose
    private Integer claimId;
    @SerializedName("sfNo")
    @Expose
    private Integer sfNo;
    @SerializedName("addOrderNo")
    @Expose
    private Integer addOrderNo;
    @SerializedName("extReference")
    @Expose
    private String extReference;
    @SerializedName("provisionList")
    @Expose
    private List<ProvisionList> provisionList = null;
    @SerializedName("processList")
    @Expose
    private List<ProcessList> processList = null;
    @SerializedName("notMatchProcessList")
    @Expose
    private List<Object> notMatchProcessList = null;
    @SerializedName("itemList")
    @Expose
    private List<Object> itemList = null;
    @SerializedName("rejectLossList")
    @Expose
    private List<Object> rejectLossList = null;
    @SerializedName("medicineList")
    @Expose
    private List<Object> medicineList = null;
    @SerializedName("vaccineList")
    @Expose
    private List<Object> vaccineList = null;
    @SerializedName("diagnosisList")
    @Expose
    private Object diagnosisList;
    @SerializedName("webAuthorizationList")
    @Expose
    private List<Object> webAuthorizationList = null;
    @SerializedName("surgeryDetailDtoList")
    @Expose
    private List<Object> surgeryDetailDtoList = null;
    @SerializedName("patientInfoForm")
    @Expose
    private Object patientInfoForm;
    @SerializedName("patientRelationDetailDto")
    @Expose
    private Object patientRelationDetailDto;
    @SerializedName("healthClaimStatusDtoList")
    @Expose
    private Object healthClaimStatusDtoList;
    @SerializedName("healthClaimStatusHistoryDtoList")
    @Expose
    private Object healthClaimStatusHistoryDtoList;
    @SerializedName("claimPolicyBasesDto")
    @Expose
    private ClaimPolicyBasesDto claimPolicyBasesDto;
    @SerializedName("claimPolOarDto")
    @Expose
    private ClaimPolOarDto claimPolOarDto;
    @SerializedName("affluentFlag")
    @Expose
    private Object affluentFlag;
    @SerializedName("beforeComplaintIllness")
    @Expose
    private Object beforeComplaintIllness;
    @SerializedName("breResultCode")
    @Expose
    private Object breResultCode;
    @SerializedName("claimInstLoc")
    @Expose
    private String claimInstLoc;
    @SerializedName("claimInstType")
    @Expose
    private String claimInstType;
    @SerializedName("classDisease1")
    @Expose
    private String classDisease1;
    @SerializedName("classDisease2")
    @Expose
    private Object classDisease2;
    @SerializedName("classDisease3")
    @Expose
    private Object classDisease3;
    @SerializedName("closeDate")
    @Expose
    private Object closeDate;
    @SerializedName("commDate")
    @Expose
    private Object commDate;
    @SerializedName("communicationExp1")
    @Expose
    private Object communicationExp1;
    @SerializedName("communicationExp2")
    @Expose
    private Object communicationExp2;
    @SerializedName("communicationNo")
    @Expose
    private Object communicationNo;
    @SerializedName("communicationNumber1")
    @Expose
    private Object communicationNumber1;
    @SerializedName("communicationNumber2")
    @Expose
    private Object communicationNumber2;
    @SerializedName("complaintStart")
    @Expose
    private Object complaintStart;
    @SerializedName("consultationDiagnosis")
    @Expose
    private Object consultationDiagnosis;
    @SerializedName("contractMessage")
    @Expose
    private Object contractMessage;
    @SerializedName("countryCode")
    @Expose
    private Object countryCode;
    @SerializedName("cpaStatus")
    @Expose
    private Object cpaStatus;
    @SerializedName("dateOfLoss")
    @Expose
    private String dateOfLoss;
    @SerializedName("deductionRate")
    @Expose
    private Object deductionRate;
    @SerializedName("dhInst")
    @Expose
    private Object dhInst;
    @SerializedName("dischargeDate")
    @Expose
    private Object dischargeDate;
    @SerializedName("doctorCode")
    @Expose
    private Object doctorCode;
    @SerializedName("doctorInfo")
    @Expose
    private Object doctorInfo;
    @SerializedName("recourseInfo")
    @Expose
    private Object recourseInfo;
    @SerializedName("doctorStatus")
    @Expose
    private Object doctorStatus;
    @SerializedName("doctorType")
    @Expose
    private String doctorType;
    @SerializedName("drDiplomaNo")
    @Expose
    private Object drDiplomaNo;
    @SerializedName("drNameLastname")
    @Expose
    private String drNameLastname;
    @SerializedName("exGratiaFee")
    @Expose
    private Object exGratiaFee;
    @SerializedName("examDate")
    @Expose
    private Object examDate;
    @SerializedName("examinationsResults")
    @Expose
    private Object examinationsResults;
    @SerializedName("explanation")
    @Expose
    private Object explanation;
    @SerializedName("extDoctorAmt")
    @Expose
    private Integer extDoctorAmt;
    @SerializedName("finalClassDisease1")
    @Expose
    private String finalClassDisease1;
    @SerializedName("finalClassDisease2")
    @Expose
    private Object finalClassDisease2;
    @SerializedName("finalClassDisease3")
    @Expose
    private Object finalClassDisease3;
    @SerializedName("explanationList")
    @Expose
    private Object explanationList;
    @SerializedName("ibanCode")
    @Expose
    private Object ibanCode;
    @SerializedName("fromCityCode")
    @Expose
    private Object fromCityCode;
    @SerializedName("fromDistrictCode")
    @Expose
    private Object fromDistrictCode;
    @SerializedName("fromToPlace")
    @Expose
    private Object fromToPlace;
    @SerializedName("groupCode")
    @Expose
    private String groupCode;
    @SerializedName("hasUnreadableDoc")
    @Expose
    private Object hasUnreadableDoc;
    @SerializedName("hlthSrvPay")
    @Expose
    private Object hlthSrvPay;
    @SerializedName("hospitalAmt")
    @Expose
    private Integer hospitalAmt;
    @SerializedName("hospitalRefNo")
    @Expose
    private Object hospitalRefNo;
    @SerializedName("hospitalizeApprFormExpl1")
    @Expose
    private Object hospitalizeApprFormExpl1;
    @SerializedName("hospitalizeApprFormExpl2")
    @Expose
    private Object hospitalizeApprFormExpl2;
    @SerializedName("hospitalizeDate")
    @Expose
    private Object hospitalizeDate;
    @SerializedName("identitiyType")
    @Expose
    private Object identitiyType;
    @SerializedName("identityNo")
    @Expose
    private String identityNo;
    @SerializedName("identityNumber")
    @Expose
    private String identityNumber;
    @SerializedName("instGlnCode")
    @Expose
    private Object instGlnCode;
    @SerializedName("instTaxNumber")
    @Expose
    private Object instTaxNumber;
    @SerializedName("instTaxOffice")
    @Expose
    private Object instTaxOffice;
    @SerializedName("instituteCode")
    @Expose
    private Integer instituteCode;
    @SerializedName("invoiceDate")
    @Expose
    private Object invoiceDate;
    @SerializedName("invoiceExplanation")
    @Expose
    private Object invoiceExplanation;
    @SerializedName("invoiceNo")
    @Expose
    private Object invoiceNo;
    @SerializedName("invoiceTotal")
    @Expose
    private Object invoiceTotal;
    @SerializedName("ahek")
    @Expose
    private Boolean ahek;
    @SerializedName("automated")
    @Expose
    private Boolean automated;
    @SerializedName("complementary")
    @Expose
    private Boolean complementary;
    @SerializedName("exGratia")
    @Expose
    private Boolean exGratia;
    @SerializedName("extDoctor")
    @Expose
    private Boolean extDoctor;
    @SerializedName("judicial")
    @Expose
    private Object judicial;
    @SerializedName("ok")
    @Expose
    private Boolean ok;
    @SerializedName("onlyExaminationFee")
    @Expose
    private Boolean onlyExaminationFee;
    @SerializedName("original")
    @Expose
    private Boolean original;
    @SerializedName("patientVisitMade")
    @Expose
    private Boolean patientVisitMade;
    @SerializedName("pregnant")
    @Expose
    private Boolean pregnant;
    @SerializedName("referral")
    @Expose
    private Boolean referral;
    @SerializedName("sgkUsed")
    @Expose
    private Object sgkUsed;
    @SerializedName("suspense")
    @Expose
    private Object suspense;
    @SerializedName("urgentCure")
    @Expose
    private Boolean urgentCure;
    @SerializedName("web")
    @Expose
    private Boolean web;
    @SerializedName("wrongProvision")
    @Expose
    private Boolean wrongProvision;
    @SerializedName("lastMenstDate")
    @Expose
    private Object lastMenstDate;
    @SerializedName("medicalBackground")
    @Expose
    private Object medicalBackground;
    @SerializedName("medulaDate")
    @Expose
    private Object medulaDate;
    @SerializedName("nationality")
    @Expose
    private Object nationality;
    @SerializedName("origClaimInstLoc")
    @Expose
    private String origClaimInstLoc;
    @SerializedName("origClaimInstType")
    @Expose
    private String origClaimInstType;
    @SerializedName("otherCountry")
    @Expose
    private Object otherCountry;
    @SerializedName("otherCountryCity")
    @Expose
    private Object otherCountryCity;
    @SerializedName("packageDate")
    @Expose
    private String packageDate;
    @SerializedName("packageId")
    @Expose
    private Integer packageId;
    @SerializedName("partId")
    @Expose
    private Integer partId;
    @SerializedName("patientAdmittanceDate")
    @Expose
    private Object patientAdmittanceDate;
    @SerializedName("patientComplaint")
    @Expose
    private Object patientComplaint;
    @SerializedName("paymentTotal")
    @Expose
    private Object paymentTotal;
    @SerializedName("personalNotes")
    @Expose
    private Object personalNotes;
    @SerializedName("plannedTreatmentProc")
    @Expose
    private Object plannedTreatmentProc;
    @SerializedName("possDischargeDate")
    @Expose
    private Object possDischargeDate;
    @SerializedName("possHospitalizeDate")
    @Expose
    private Object possHospitalizeDate;
    @SerializedName("prediagnosisDiagnosis")
    @Expose
    private Object prediagnosisDiagnosis;
    @SerializedName("prescriptionDate")
    @Expose
    private Object prescriptionDate;
    @SerializedName("prescriptionNo")
    @Expose
    private Object prescriptionNo;
    @SerializedName("prescriptionType")
    @Expose
    private Object prescriptionType;
    @SerializedName("processDate")
    @Expose
    private String processDate;
    @SerializedName("processExp")
    @Expose
    private Object processExp;
    @SerializedName("provDemandDate")
    @Expose
    private Object provDemandDate;
    @SerializedName("provisionDate")
    @Expose
    private String provisionDate;
    @SerializedName("provisionFeeChargeDate")
    @Expose
    private Object provisionFeeChargeDate;
    @SerializedName("provisionUserId")
    @Expose
    private String provisionUserId;
    @SerializedName("realizationDate")
    @Expose
    private String realizationDate;
    @SerializedName("referralInstituteName")
    @Expose
    private Object referralInstituteName;
    @SerializedName("relClaimId")
    @Expose
    private Object relClaimId;
    @SerializedName("requestAmount")
    @Expose
    private Integer requestAmount;
    @SerializedName("requestSystem")
    @Expose
    private String requestSystem;
    @SerializedName("sgkRefNo")
    @Expose
    private Object sgkRefNo;
    @SerializedName("sgkTotal")
    @Expose
    private Integer sgkTotal;
    @SerializedName("shippingDate")
    @Expose
    private Object shippingDate;
    @SerializedName("shippingNo")
    @Expose
    private Object shippingNo;
    @SerializedName("socSecInstApp")
    @Expose
    private Object socSecInstApp;
    @SerializedName("specialtySubject")
    @Expose
    private String specialtySubject;
    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("surgeryDate")
    @Expose
    private Object surgeryDate;
    @SerializedName("suspenseDate")
    @Expose
    private Object suspenseDate;
    @SerializedName("swiftCode")
    @Expose
    private String swiftCode;
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("toCityCode")
    @Expose
    private Object toCityCode;
    @SerializedName("toDistrictCode")
    @Expose
    private Object toDistrictCode;
    @SerializedName("visitingReason")
    @Expose
    private Object visitingReason;
    @SerializedName("webLocationCode")
    @Expose
    private Object webLocationCode;
    @SerializedName("ytType")
    @Expose
    private Object ytType;
    @SerializedName("versionNo")
    @Expose
    private Object versionNo;
    @SerializedName("updateLimit")
    @Expose
    private Boolean updateLimit;
    @SerializedName("breDecisionLogList")
    @Expose
    private List<Object> breDecisionLogList = null;
    @SerializedName("previousStatusCode")
    @Expose
    private String previousStatusCode;
    @SerializedName("denySms")
    @Expose
    private Boolean denySms;
    @SerializedName("verifiedFromMedula")
    @Expose
    private Object verifiedFromMedula;
    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("duplicateProvisionList")
    @Expose
    private List<Object> duplicateProvisionList = null;
    @SerializedName("duplicatePharmacyList")
    @Expose
    private List<Object> duplicatePharmacyList = null;
    @SerializedName("sumInstRequestAmount")
    @Expose
    private Integer sumInstRequestAmount;
    @SerializedName("sumSysRequestAmount")
    @Expose
    private Integer sumSysRequestAmount;
    @SerializedName("sumRequestAmount")
    @Expose
    private Integer sumRequestAmount;
    @SerializedName("sumRefusalAmount")
    @Expose
    private Integer sumRefusalAmount;
    @SerializedName("sumExemptionAmount")
    @Expose
    private Integer sumExemptionAmount;
    @SerializedName("sumSgkAmount")
    @Expose
    private Integer sumSgkAmount;
    @SerializedName("sumInsuredAmount")
    @Expose
    private Integer sumInsuredAmount;
    @SerializedName("sumInstWrongAmount")
    @Expose
    private Integer sumInstWrongAmount;
    @SerializedName("sumNetProvisionAmount")
    @Expose
    private Integer sumNetProvisionAmount;
    @SerializedName("sumCancelAmount")
    @Expose
    private Integer sumCancelAmount;
    @SerializedName("totalInsuredAmount")
    @Expose
    private Integer totalInsuredAmount;
    @SerializedName("eprescriptionNo")
    @Expose
    private Object eprescriptionNo;

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }

    public Integer getSfNo() {
        return sfNo;
    }

    public void setSfNo(Integer sfNo) {
        this.sfNo = sfNo;
    }

    public Integer getAddOrderNo() {
        return addOrderNo;
    }

    public void setAddOrderNo(Integer addOrderNo) {
        this.addOrderNo = addOrderNo;
    }

    public String getExtReference() {
        return extReference;
    }

    public void setExtReference(String extReference) {
        this.extReference = extReference;
    }

    public List<ProvisionList> getProvisionList() {
        return provisionList;
    }

    public void setProvisionList(List<ProvisionList> provisionList) {
        this.provisionList = provisionList;
    }

    public List<ProcessList> getProcessList() {
        return processList;
    }

    public void setProcessList(List<ProcessList> processList) {
        this.processList = processList;
    }

    public List<Object> getNotMatchProcessList() {
        return notMatchProcessList;
    }

    public void setNotMatchProcessList(List<Object> notMatchProcessList) {
        this.notMatchProcessList = notMatchProcessList;
    }

    public List<Object> getItemList() {
        return itemList;
    }

    public void setItemList(List<Object> itemList) {
        this.itemList = itemList;
    }

    public List<Object> getRejectLossList() {
        return rejectLossList;
    }

    public void setRejectLossList(List<Object> rejectLossList) {
        this.rejectLossList = rejectLossList;
    }

    public List<Object> getMedicineList() {
        return medicineList;
    }

    public void setMedicineList(List<Object> medicineList) {
        this.medicineList = medicineList;
    }

    public List<Object> getVaccineList() {
        return vaccineList;
    }

    public void setVaccineList(List<Object> vaccineList) {
        this.vaccineList = vaccineList;
    }

    public Object getDiagnosisList() {
        return diagnosisList;
    }

    public void setDiagnosisList(Object diagnosisList) {
        this.diagnosisList = diagnosisList;
    }

    public List<Object> getWebAuthorizationList() {
        return webAuthorizationList;
    }

    public void setWebAuthorizationList(List<Object> webAuthorizationList) {
        this.webAuthorizationList = webAuthorizationList;
    }

    public List<Object> getSurgeryDetailDtoList() {
        return surgeryDetailDtoList;
    }

    public void setSurgeryDetailDtoList(List<Object> surgeryDetailDtoList) {
        this.surgeryDetailDtoList = surgeryDetailDtoList;
    }

    public Object getPatientInfoForm() {
        return patientInfoForm;
    }

    public void setPatientInfoForm(Object patientInfoForm) {
        this.patientInfoForm = patientInfoForm;
    }

    public Object getPatientRelationDetailDto() {
        return patientRelationDetailDto;
    }

    public void setPatientRelationDetailDto(Object patientRelationDetailDto) {
        this.patientRelationDetailDto = patientRelationDetailDto;
    }

    public Object getHealthClaimStatusDtoList() {
        return healthClaimStatusDtoList;
    }

    public void setHealthClaimStatusDtoList(Object healthClaimStatusDtoList) {
        this.healthClaimStatusDtoList = healthClaimStatusDtoList;
    }

    public Object getHealthClaimStatusHistoryDtoList() {
        return healthClaimStatusHistoryDtoList;
    }

    public void setHealthClaimStatusHistoryDtoList(Object healthClaimStatusHistoryDtoList) {
        this.healthClaimStatusHistoryDtoList = healthClaimStatusHistoryDtoList;
    }

    public ClaimPolicyBasesDto getClaimPolicyBasesDto() {
        return claimPolicyBasesDto;
    }

    public void setClaimPolicyBasesDto(ClaimPolicyBasesDto claimPolicyBasesDto) {
        this.claimPolicyBasesDto = claimPolicyBasesDto;
    }

    public ClaimPolOarDto getClaimPolOarDto() {
        return claimPolOarDto;
    }

    public void setClaimPolOarDto(ClaimPolOarDto claimPolOarDto) {
        this.claimPolOarDto = claimPolOarDto;
    }

    public Object getAffluentFlag() {
        return affluentFlag;
    }

    public void setAffluentFlag(Object affluentFlag) {
        this.affluentFlag = affluentFlag;
    }

    public Object getBeforeComplaintIllness() {
        return beforeComplaintIllness;
    }

    public void setBeforeComplaintIllness(Object beforeComplaintIllness) {
        this.beforeComplaintIllness = beforeComplaintIllness;
    }

    public Object getBreResultCode() {
        return breResultCode;
    }

    public void setBreResultCode(Object breResultCode) {
        this.breResultCode = breResultCode;
    }

    public String getClaimInstLoc() {
        return claimInstLoc;
    }

    public void setClaimInstLoc(String claimInstLoc) {
        this.claimInstLoc = claimInstLoc;
    }

    public String getClaimInstType() {
        return claimInstType;
    }

    public void setClaimInstType(String claimInstType) {
        this.claimInstType = claimInstType;
    }

    public String getClassDisease1() {
        return classDisease1;
    }

    public void setClassDisease1(String classDisease1) {
        this.classDisease1 = classDisease1;
    }

    public Object getClassDisease2() {
        return classDisease2;
    }

    public void setClassDisease2(Object classDisease2) {
        this.classDisease2 = classDisease2;
    }

    public Object getClassDisease3() {
        return classDisease3;
    }

    public void setClassDisease3(Object classDisease3) {
        this.classDisease3 = classDisease3;
    }

    public Object getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Object closeDate) {
        this.closeDate = closeDate;
    }

    public Object getCommDate() {
        return commDate;
    }

    public void setCommDate(Object commDate) {
        this.commDate = commDate;
    }

    public Object getCommunicationExp1() {
        return communicationExp1;
    }

    public void setCommunicationExp1(Object communicationExp1) {
        this.communicationExp1 = communicationExp1;
    }

    public Object getCommunicationExp2() {
        return communicationExp2;
    }

    public void setCommunicationExp2(Object communicationExp2) {
        this.communicationExp2 = communicationExp2;
    }

    public Object getCommunicationNo() {
        return communicationNo;
    }

    public void setCommunicationNo(Object communicationNo) {
        this.communicationNo = communicationNo;
    }

    public Object getCommunicationNumber1() {
        return communicationNumber1;
    }

    public void setCommunicationNumber1(Object communicationNumber1) {
        this.communicationNumber1 = communicationNumber1;
    }

    public Object getCommunicationNumber2() {
        return communicationNumber2;
    }

    public void setCommunicationNumber2(Object communicationNumber2) {
        this.communicationNumber2 = communicationNumber2;
    }

    public Object getComplaintStart() {
        return complaintStart;
    }

    public void setComplaintStart(Object complaintStart) {
        this.complaintStart = complaintStart;
    }

    public Object getConsultationDiagnosis() {
        return consultationDiagnosis;
    }

    public void setConsultationDiagnosis(Object consultationDiagnosis) {
        this.consultationDiagnosis = consultationDiagnosis;
    }

    public Object getContractMessage() {
        return contractMessage;
    }

    public void setContractMessage(Object contractMessage) {
        this.contractMessage = contractMessage;
    }

    public Object getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Object countryCode) {
        this.countryCode = countryCode;
    }

    public Object getCpaStatus() {
        return cpaStatus;
    }

    public void setCpaStatus(Object cpaStatus) {
        this.cpaStatus = cpaStatus;
    }

    public String getDateOfLoss() {
        return dateOfLoss;
    }

    public void setDateOfLoss(String dateOfLoss) {
        this.dateOfLoss = dateOfLoss;
    }

    public Object getDeductionRate() {
        return deductionRate;
    }

    public void setDeductionRate(Object deductionRate) {
        this.deductionRate = deductionRate;
    }

    public Object getDhInst() {
        return dhInst;
    }

    public void setDhInst(Object dhInst) {
        this.dhInst = dhInst;
    }

    public Object getDischargeDate() {
        return dischargeDate;
    }

    public void setDischargeDate(Object dischargeDate) {
        this.dischargeDate = dischargeDate;
    }

    public Object getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(Object doctorCode) {
        this.doctorCode = doctorCode;
    }

    public Object getDoctorInfo() {
        return doctorInfo;
    }

    public void setDoctorInfo(Object doctorInfo) {
        this.doctorInfo = doctorInfo;
    }

    public Object getRecourseInfo() {
        return recourseInfo;
    }

    public void setRecourseInfo(Object recourseInfo) {
        this.recourseInfo = recourseInfo;
    }

    public Object getDoctorStatus() {
        return doctorStatus;
    }

    public void setDoctorStatus(Object doctorStatus) {
        this.doctorStatus = doctorStatus;
    }

    public String getDoctorType() {
        return doctorType;
    }

    public void setDoctorType(String doctorType) {
        this.doctorType = doctorType;
    }

    public Object getDrDiplomaNo() {
        return drDiplomaNo;
    }

    public void setDrDiplomaNo(Object drDiplomaNo) {
        this.drDiplomaNo = drDiplomaNo;
    }

    public String getDrNameLastname() {
        return drNameLastname;
    }

    public void setDrNameLastname(String drNameLastname) {
        this.drNameLastname = drNameLastname;
    }

    public Object getExGratiaFee() {
        return exGratiaFee;
    }

    public void setExGratiaFee(Object exGratiaFee) {
        this.exGratiaFee = exGratiaFee;
    }

    public Object getExamDate() {
        return examDate;
    }

    public void setExamDate(Object examDate) {
        this.examDate = examDate;
    }

    public Object getExaminationsResults() {
        return examinationsResults;
    }

    public void setExaminationsResults(Object examinationsResults) {
        this.examinationsResults = examinationsResults;
    }

    public Object getExplanation() {
        return explanation;
    }

    public void setExplanation(Object explanation) {
        this.explanation = explanation;
    }

    public Integer getExtDoctorAmt() {
        return extDoctorAmt;
    }

    public void setExtDoctorAmt(Integer extDoctorAmt) {
        this.extDoctorAmt = extDoctorAmt;
    }

    public String getFinalClassDisease1() {
        return finalClassDisease1;
    }

    public void setFinalClassDisease1(String finalClassDisease1) {
        this.finalClassDisease1 = finalClassDisease1;
    }

    public Object getFinalClassDisease2() {
        return finalClassDisease2;
    }

    public void setFinalClassDisease2(Object finalClassDisease2) {
        this.finalClassDisease2 = finalClassDisease2;
    }

    public Object getFinalClassDisease3() {
        return finalClassDisease3;
    }

    public void setFinalClassDisease3(Object finalClassDisease3) {
        this.finalClassDisease3 = finalClassDisease3;
    }

    public Object getExplanationList() {
        return explanationList;
    }

    public void setExplanationList(Object explanationList) {
        this.explanationList = explanationList;
    }

    public Object getIbanCode() {
        return ibanCode;
    }

    public void setIbanCode(Object ibanCode) {
        this.ibanCode = ibanCode;
    }

    public Object getFromCityCode() {
        return fromCityCode;
    }

    public void setFromCityCode(Object fromCityCode) {
        this.fromCityCode = fromCityCode;
    }

    public Object getFromDistrictCode() {
        return fromDistrictCode;
    }

    public void setFromDistrictCode(Object fromDistrictCode) {
        this.fromDistrictCode = fromDistrictCode;
    }

    public Object getFromToPlace() {
        return fromToPlace;
    }

    public void setFromToPlace(Object fromToPlace) {
        this.fromToPlace = fromToPlace;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public Object getHasUnreadableDoc() {
        return hasUnreadableDoc;
    }

    public void setHasUnreadableDoc(Object hasUnreadableDoc) {
        this.hasUnreadableDoc = hasUnreadableDoc;
    }

    public Object getHlthSrvPay() {
        return hlthSrvPay;
    }

    public void setHlthSrvPay(Object hlthSrvPay) {
        this.hlthSrvPay = hlthSrvPay;
    }

    public Integer getHospitalAmt() {
        return hospitalAmt;
    }

    public void setHospitalAmt(Integer hospitalAmt) {
        this.hospitalAmt = hospitalAmt;
    }

    public Object getHospitalRefNo() {
        return hospitalRefNo;
    }

    public void setHospitalRefNo(Object hospitalRefNo) {
        this.hospitalRefNo = hospitalRefNo;
    }

    public Object getHospitalizeApprFormExpl1() {
        return hospitalizeApprFormExpl1;
    }

    public void setHospitalizeApprFormExpl1(Object hospitalizeApprFormExpl1) {
        this.hospitalizeApprFormExpl1 = hospitalizeApprFormExpl1;
    }

    public Object getHospitalizeApprFormExpl2() {
        return hospitalizeApprFormExpl2;
    }

    public void setHospitalizeApprFormExpl2(Object hospitalizeApprFormExpl2) {
        this.hospitalizeApprFormExpl2 = hospitalizeApprFormExpl2;
    }

    public Object getHospitalizeDate() {
        return hospitalizeDate;
    }

    public void setHospitalizeDate(Object hospitalizeDate) {
        this.hospitalizeDate = hospitalizeDate;
    }

    public Object getIdentitiyType() {
        return identitiyType;
    }

    public void setIdentitiyType(Object identitiyType) {
        this.identitiyType = identitiyType;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public Object getInstGlnCode() {
        return instGlnCode;
    }

    public void setInstGlnCode(Object instGlnCode) {
        this.instGlnCode = instGlnCode;
    }

    public Object getInstTaxNumber() {
        return instTaxNumber;
    }

    public void setInstTaxNumber(Object instTaxNumber) {
        this.instTaxNumber = instTaxNumber;
    }

    public Object getInstTaxOffice() {
        return instTaxOffice;
    }

    public void setInstTaxOffice(Object instTaxOffice) {
        this.instTaxOffice = instTaxOffice;
    }

    public Integer getInstituteCode() {
        return instituteCode;
    }

    public void setInstituteCode(Integer instituteCode) {
        this.instituteCode = instituteCode;
    }

    public Object getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Object invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Object getInvoiceExplanation() {
        return invoiceExplanation;
    }

    public void setInvoiceExplanation(Object invoiceExplanation) {
        this.invoiceExplanation = invoiceExplanation;
    }

    public Object getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(Object invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public Object getInvoiceTotal() {
        return invoiceTotal;
    }

    public void setInvoiceTotal(Object invoiceTotal) {
        this.invoiceTotal = invoiceTotal;
    }

    public Boolean getAhek() {
        return ahek;
    }

    public void setAhek(Boolean ahek) {
        this.ahek = ahek;
    }

    public Boolean getAutomated() {
        return automated;
    }

    public void setAutomated(Boolean automated) {
        this.automated = automated;
    }

    public Boolean getComplementary() {
        return complementary;
    }

    public void setComplementary(Boolean complementary) {
        this.complementary = complementary;
    }

    public Boolean getExGratia() {
        return exGratia;
    }

    public void setExGratia(Boolean exGratia) {
        this.exGratia = exGratia;
    }

    public Boolean getExtDoctor() {
        return extDoctor;
    }

    public void setExtDoctor(Boolean extDoctor) {
        this.extDoctor = extDoctor;
    }

    public Object getJudicial() {
        return judicial;
    }

    public void setJudicial(Object judicial) {
        this.judicial = judicial;
    }

    public Boolean getOk() {
        return ok;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }

    public Boolean getOnlyExaminationFee() {
        return onlyExaminationFee;
    }

    public void setOnlyExaminationFee(Boolean onlyExaminationFee) {
        this.onlyExaminationFee = onlyExaminationFee;
    }

    public Boolean getOriginal() {
        return original;
    }

    public void setOriginal(Boolean original) {
        this.original = original;
    }

    public Boolean getPatientVisitMade() {
        return patientVisitMade;
    }

    public void setPatientVisitMade(Boolean patientVisitMade) {
        this.patientVisitMade = patientVisitMade;
    }

    public Boolean getPregnant() {
        return pregnant;
    }

    public void setPregnant(Boolean pregnant) {
        this.pregnant = pregnant;
    }

    public Boolean getReferral() {
        return referral;
    }

    public void setReferral(Boolean referral) {
        this.referral = referral;
    }

    public Object getSgkUsed() {
        return sgkUsed;
    }

    public void setSgkUsed(Object sgkUsed) {
        this.sgkUsed = sgkUsed;
    }

    public Object getSuspense() {
        return suspense;
    }

    public void setSuspense(Object suspense) {
        this.suspense = suspense;
    }

    public Boolean getUrgentCure() {
        return urgentCure;
    }

    public void setUrgentCure(Boolean urgentCure) {
        this.urgentCure = urgentCure;
    }

    public Boolean getWeb() {
        return web;
    }

    public void setWeb(Boolean web) {
        this.web = web;
    }

    public Boolean getWrongProvision() {
        return wrongProvision;
    }

    public void setWrongProvision(Boolean wrongProvision) {
        this.wrongProvision = wrongProvision;
    }

    public Object getLastMenstDate() {
        return lastMenstDate;
    }

    public void setLastMenstDate(Object lastMenstDate) {
        this.lastMenstDate = lastMenstDate;
    }

    public Object getMedicalBackground() {
        return medicalBackground;
    }

    public void setMedicalBackground(Object medicalBackground) {
        this.medicalBackground = medicalBackground;
    }

    public Object getMedulaDate() {
        return medulaDate;
    }

    public void setMedulaDate(Object medulaDate) {
        this.medulaDate = medulaDate;
    }

    public Object getNationality() {
        return nationality;
    }

    public void setNationality(Object nationality) {
        this.nationality = nationality;
    }

    public String getOrigClaimInstLoc() {
        return origClaimInstLoc;
    }

    public void setOrigClaimInstLoc(String origClaimInstLoc) {
        this.origClaimInstLoc = origClaimInstLoc;
    }

    public String getOrigClaimInstType() {
        return origClaimInstType;
    }

    public void setOrigClaimInstType(String origClaimInstType) {
        this.origClaimInstType = origClaimInstType;
    }

    public Object getOtherCountry() {
        return otherCountry;
    }

    public void setOtherCountry(Object otherCountry) {
        this.otherCountry = otherCountry;
    }

    public Object getOtherCountryCity() {
        return otherCountryCity;
    }

    public void setOtherCountryCity(Object otherCountryCity) {
        this.otherCountryCity = otherCountryCity;
    }

    public String getPackageDate() {
        return packageDate;
    }

    public void setPackageDate(String packageDate) {
        this.packageDate = packageDate;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public Integer getPartId() {
        return partId;
    }

    public void setPartId(Integer partId) {
        this.partId = partId;
    }

    public Object getPatientAdmittanceDate() {
        return patientAdmittanceDate;
    }

    public void setPatientAdmittanceDate(Object patientAdmittanceDate) {
        this.patientAdmittanceDate = patientAdmittanceDate;
    }

    public Object getPatientComplaint() {
        return patientComplaint;
    }

    public void setPatientComplaint(Object patientComplaint) {
        this.patientComplaint = patientComplaint;
    }

    public Object getPaymentTotal() {
        return paymentTotal;
    }

    public void setPaymentTotal(Object paymentTotal) {
        this.paymentTotal = paymentTotal;
    }

    public Object getPersonalNotes() {
        return personalNotes;
    }

    public void setPersonalNotes(Object personalNotes) {
        this.personalNotes = personalNotes;
    }

    public Object getPlannedTreatmentProc() {
        return plannedTreatmentProc;
    }

    public void setPlannedTreatmentProc(Object plannedTreatmentProc) {
        this.plannedTreatmentProc = plannedTreatmentProc;
    }

    public Object getPossDischargeDate() {
        return possDischargeDate;
    }

    public void setPossDischargeDate(Object possDischargeDate) {
        this.possDischargeDate = possDischargeDate;
    }

    public Object getPossHospitalizeDate() {
        return possHospitalizeDate;
    }

    public void setPossHospitalizeDate(Object possHospitalizeDate) {
        this.possHospitalizeDate = possHospitalizeDate;
    }

    public Object getPrediagnosisDiagnosis() {
        return prediagnosisDiagnosis;
    }

    public void setPrediagnosisDiagnosis(Object prediagnosisDiagnosis) {
        this.prediagnosisDiagnosis = prediagnosisDiagnosis;
    }

    public Object getPrescriptionDate() {
        return prescriptionDate;
    }

    public void setPrescriptionDate(Object prescriptionDate) {
        this.prescriptionDate = prescriptionDate;
    }

    public Object getPrescriptionNo() {
        return prescriptionNo;
    }

    public void setPrescriptionNo(Object prescriptionNo) {
        this.prescriptionNo = prescriptionNo;
    }

    public Object getPrescriptionType() {
        return prescriptionType;
    }

    public void setPrescriptionType(Object prescriptionType) {
        this.prescriptionType = prescriptionType;
    }

    public String getProcessDate() {
        return processDate;
    }

    public void setProcessDate(String processDate) {
        this.processDate = processDate;
    }

    public Object getProcessExp() {
        return processExp;
    }

    public void setProcessExp(Object processExp) {
        this.processExp = processExp;
    }

    public Object getProvDemandDate() {
        return provDemandDate;
    }

    public void setProvDemandDate(Object provDemandDate) {
        this.provDemandDate = provDemandDate;
    }

    public String getProvisionDate() {
        return provisionDate;
    }

    public void setProvisionDate(String provisionDate) {
        this.provisionDate = provisionDate;
    }

    public Object getProvisionFeeChargeDate() {
        return provisionFeeChargeDate;
    }

    public void setProvisionFeeChargeDate(Object provisionFeeChargeDate) {
        this.provisionFeeChargeDate = provisionFeeChargeDate;
    }

    public String getProvisionUserId() {
        return provisionUserId;
    }

    public void setProvisionUserId(String provisionUserId) {
        this.provisionUserId = provisionUserId;
    }

    public String getRealizationDate() {
        return realizationDate;
    }

    public void setRealizationDate(String realizationDate) {
        this.realizationDate = realizationDate;
    }

    public Object getReferralInstituteName() {
        return referralInstituteName;
    }

    public void setReferralInstituteName(Object referralInstituteName) {
        this.referralInstituteName = referralInstituteName;
    }

    public Object getRelClaimId() {
        return relClaimId;
    }

    public void setRelClaimId(Object relClaimId) {
        this.relClaimId = relClaimId;
    }

    public Integer getRequestAmount() {
        return requestAmount;
    }

    public void setRequestAmount(Integer requestAmount) {
        this.requestAmount = requestAmount;
    }

    public String getRequestSystem() {
        return requestSystem;
    }

    public void setRequestSystem(String requestSystem) {
        this.requestSystem = requestSystem;
    }

    public Object getSgkRefNo() {
        return sgkRefNo;
    }

    public void setSgkRefNo(Object sgkRefNo) {
        this.sgkRefNo = sgkRefNo;
    }

    public Integer getSgkTotal() {
        return sgkTotal;
    }

    public void setSgkTotal(Integer sgkTotal) {
        this.sgkTotal = sgkTotal;
    }

    public Object getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(Object shippingDate) {
        this.shippingDate = shippingDate;
    }

    public Object getShippingNo() {
        return shippingNo;
    }

    public void setShippingNo(Object shippingNo) {
        this.shippingNo = shippingNo;
    }

    public Object getSocSecInstApp() {
        return socSecInstApp;
    }

    public void setSocSecInstApp(Object socSecInstApp) {
        this.socSecInstApp = socSecInstApp;
    }

    public String getSpecialtySubject() {
        return specialtySubject;
    }

    public void setSpecialtySubject(String specialtySubject) {
        this.specialtySubject = specialtySubject;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Object getSurgeryDate() {
        return surgeryDate;
    }

    public void setSurgeryDate(Object surgeryDate) {
        this.surgeryDate = surgeryDate;
    }

    public Object getSuspenseDate() {
        return suspenseDate;
    }

    public void setSuspenseDate(Object suspenseDate) {
        this.suspenseDate = suspenseDate;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Object getToCityCode() {
        return toCityCode;
    }

    public void setToCityCode(Object toCityCode) {
        this.toCityCode = toCityCode;
    }

    public Object getToDistrictCode() {
        return toDistrictCode;
    }

    public void setToDistrictCode(Object toDistrictCode) {
        this.toDistrictCode = toDistrictCode;
    }

    public Object getVisitingReason() {
        return visitingReason;
    }

    public void setVisitingReason(Object visitingReason) {
        this.visitingReason = visitingReason;
    }

    public Object getWebLocationCode() {
        return webLocationCode;
    }

    public void setWebLocationCode(Object webLocationCode) {
        this.webLocationCode = webLocationCode;
    }

    public Object getYtType() {
        return ytType;
    }

    public void setYtType(Object ytType) {
        this.ytType = ytType;
    }

    public Object getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(Object versionNo) {
        this.versionNo = versionNo;
    }

    public Boolean getUpdateLimit() {
        return updateLimit;
    }

    public void setUpdateLimit(Boolean updateLimit) {
        this.updateLimit = updateLimit;
    }

    public List<Object> getBreDecisionLogList() {
        return breDecisionLogList;
    }

    public void setBreDecisionLogList(List<Object> breDecisionLogList) {
        this.breDecisionLogList = breDecisionLogList;
    }

    public String getPreviousStatusCode() {
        return previousStatusCode;
    }

    public void setPreviousStatusCode(String previousStatusCode) {
        this.previousStatusCode = previousStatusCode;
    }

    public Boolean getDenySms() {
        return denySms;
    }

    public void setDenySms(Boolean denySms) {
        this.denySms = denySms;
    }

    public Object getVerifiedFromMedula() {
        return verifiedFromMedula;
    }

    public void setVerifiedFromMedula(Object verifiedFromMedula) {
        this.verifiedFromMedula = verifiedFromMedula;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public List<Object> getDuplicateProvisionList() {
        return duplicateProvisionList;
    }

    public void setDuplicateProvisionList(List<Object> duplicateProvisionList) {
        this.duplicateProvisionList = duplicateProvisionList;
    }

    public List<Object> getDuplicatePharmacyList() {
        return duplicatePharmacyList;
    }

    public void setDuplicatePharmacyList(List<Object> duplicatePharmacyList) {
        this.duplicatePharmacyList = duplicatePharmacyList;
    }

    public Integer getSumInstRequestAmount() {
        return sumInstRequestAmount;
    }

    public void setSumInstRequestAmount(Integer sumInstRequestAmount) {
        this.sumInstRequestAmount = sumInstRequestAmount;
    }

    public Integer getSumSysRequestAmount() {
        return sumSysRequestAmount;
    }

    public void setSumSysRequestAmount(Integer sumSysRequestAmount) {
        this.sumSysRequestAmount = sumSysRequestAmount;
    }

    public Integer getSumRequestAmount() {
        return sumRequestAmount;
    }

    public void setSumRequestAmount(Integer sumRequestAmount) {
        this.sumRequestAmount = sumRequestAmount;
    }

    public Integer getSumRefusalAmount() {
        return sumRefusalAmount;
    }

    public void setSumRefusalAmount(Integer sumRefusalAmount) {
        this.sumRefusalAmount = sumRefusalAmount;
    }

    public Integer getSumExemptionAmount() {
        return sumExemptionAmount;
    }

    public void setSumExemptionAmount(Integer sumExemptionAmount) {
        this.sumExemptionAmount = sumExemptionAmount;
    }

    public Integer getSumSgkAmount() {
        return sumSgkAmount;
    }

    public void setSumSgkAmount(Integer sumSgkAmount) {
        this.sumSgkAmount = sumSgkAmount;
    }

    public Integer getSumInsuredAmount() {
        return sumInsuredAmount;
    }

    public void setSumInsuredAmount(Integer sumInsuredAmount) {
        this.sumInsuredAmount = sumInsuredAmount;
    }

    public Integer getSumInstWrongAmount() {
        return sumInstWrongAmount;
    }

    public void setSumInstWrongAmount(Integer sumInstWrongAmount) {
        this.sumInstWrongAmount = sumInstWrongAmount;
    }

    public Integer getSumNetProvisionAmount() {
        return sumNetProvisionAmount;
    }

    public void setSumNetProvisionAmount(Integer sumNetProvisionAmount) {
        this.sumNetProvisionAmount = sumNetProvisionAmount;
    }

    public Integer getSumCancelAmount() {
        return sumCancelAmount;
    }

    public void setSumCancelAmount(Integer sumCancelAmount) {
        this.sumCancelAmount = sumCancelAmount;
    }

    public Integer getTotalInsuredAmount() {
        return totalInsuredAmount;
    }

    public void setTotalInsuredAmount(Integer totalInsuredAmount) {
        this.totalInsuredAmount = totalInsuredAmount;
    }

    public Object getEprescriptionNo() {
        return eprescriptionNo;
    }

    public void setEprescriptionNo(Object eprescriptionNo) {
        this.eprescriptionNo = eprescriptionNo;
    }

}
