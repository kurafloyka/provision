
package Models.load;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClaimPolicyBasesDto {

    @SerializedName("claimId")
    @Expose
    private Integer claimId;
    @SerializedName("sfNo")
    @Expose
    private Object sfNo;
    @SerializedName("addOrderNo")
    @Expose
    private Object addOrderNo;
    @SerializedName("extReference")
    @Expose
    private Object extReference;
    @SerializedName("contractId")
    @Expose
    private Integer contractId;
    @SerializedName("agentRole")
    @Expose
    private Integer agentRole;
    @SerializedName("companyId")
    @Expose
    private Object companyId;
    @SerializedName("contractStatus")
    @Expose
    private String contractStatus;
    @SerializedName("dateOfLoss")
    @Expose
    private String dateOfLoss;
    @SerializedName("extRefNo")
    @Expose
    private Object extRefNo;
    @SerializedName("groupId")
    @Expose
    private Object groupId;
    @SerializedName("policyRef")
    @Expose
    private String policyRef;
    @SerializedName("prodVersion")
    @Expose
    private Object prodVersion;
    @SerializedName("productId")
    @Expose
    private Integer productId;
    @SerializedName("termEndDate")
    @Expose
    private String termEndDate;
    @SerializedName("termStartDate")
    @Expose
    private String termStartDate;
    @SerializedName("versionNo")
    @Expose
    private Integer versionNo;

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }

    public Object getSfNo() {
        return sfNo;
    }

    public void setSfNo(Object sfNo) {
        this.sfNo = sfNo;
    }

    public Object getAddOrderNo() {
        return addOrderNo;
    }

    public void setAddOrderNo(Object addOrderNo) {
        this.addOrderNo = addOrderNo;
    }

    public Object getExtReference() {
        return extReference;
    }

    public void setExtReference(Object extReference) {
        this.extReference = extReference;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Integer getAgentRole() {
        return agentRole;
    }

    public void setAgentRole(Integer agentRole) {
        this.agentRole = agentRole;
    }

    public Object getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Object companyId) {
        this.companyId = companyId;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public String getDateOfLoss() {
        return dateOfLoss;
    }

    public void setDateOfLoss(String dateOfLoss) {
        this.dateOfLoss = dateOfLoss;
    }

    public Object getExtRefNo() {
        return extRefNo;
    }

    public void setExtRefNo(Object extRefNo) {
        this.extRefNo = extRefNo;
    }

    public Object getGroupId() {
        return groupId;
    }

    public void setGroupId(Object groupId) {
        this.groupId = groupId;
    }

    public String getPolicyRef() {
        return policyRef;
    }

    public void setPolicyRef(String policyRef) {
        this.policyRef = policyRef;
    }

    public Object getProdVersion() {
        return prodVersion;
    }

    public void setProdVersion(Object prodVersion) {
        this.prodVersion = prodVersion;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getTermEndDate() {
        return termEndDate;
    }

    public void setTermEndDate(String termEndDate) {
        this.termEndDate = termEndDate;
    }

    public String getTermStartDate() {
        return termStartDate;
    }

    public void setTermStartDate(String termStartDate) {
        this.termStartDate = termStartDate;
    }

    public Integer getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(Integer versionNo) {
        this.versionNo = versionNo;
    }

}
