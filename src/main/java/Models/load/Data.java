
package Models.load;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("healthClaimDetailDto")
    @Expose
    private HealthClaimDetailDto healthClaimDetailDto;

    public HealthClaimDetailDto getHealthClaimDetailDto() {
        return healthClaimDetailDto;
    }

    public void setHealthClaimDetailDto(HealthClaimDetailDto healthClaimDetailDto) {
        this.healthClaimDetailDto = healthClaimDetailDto;
    }

}
