
package Models.load;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProvisionList {

    @SerializedName("claimId")
    @Expose
    private Integer claimId;
    @SerializedName("sfNo")
    @Expose
    private Integer sfNo;
    @SerializedName("addOrderNo")
    @Expose
    private Integer addOrderNo;
    @SerializedName("extReference")
    @Expose
    private Object extReference;
    @SerializedName("locationCode")
    @Expose
    private Integer locationCode;
    @SerializedName("coverCode")
    @Expose
    private String coverCode;
    @SerializedName("coverCodeExplanation")
    @Expose
    private String coverCodeExplanation;
    @SerializedName("addProcAmount")
    @Expose
    private Object addProcAmount;
    @SerializedName("breResultCode")
    @Expose
    private Object breResultCode;
    @SerializedName("countryCode")
    @Expose
    private Object countryCode;
    @SerializedName("coverDistributionAmount")
    @Expose
    private Object coverDistributionAmount;
    @SerializedName("daySeance")
    @Expose
    private Integer daySeance;
    @SerializedName("doctorCode")
    @Expose
    private Object doctorCode;
    @SerializedName("doctorStatus")
    @Expose
    private Object doctorStatus;
    @SerializedName("entryDate")
    @Expose
    private Object entryDate;
    @SerializedName("exemptionAmount")
    @Expose
    private Integer exemptionAmount;
    @SerializedName("exemptionRate")
    @Expose
    private Double exemptionRate;
    @SerializedName("instAddProcAmount")
    @Expose
    private Object instAddProcAmount;
    @SerializedName("instExemptionAmount")
    @Expose
    private Integer instExemptionAmount;
    @SerializedName("instRequestAmount")
    @Expose
    private Integer instRequestAmount;
    @SerializedName("instWrongAmount")
    @Expose
    private Integer instWrongAmount;
    @SerializedName("isExGratia")
    @Expose
    private Object isExGratia;
    @SerializedName("isPoolCover")
    @Expose
    private Integer isPoolCover;
    @SerializedName("isSpecialCover")
    @Expose
    private Integer isSpecialCover;
    @SerializedName("orderNo")
    @Expose
    private Object orderNo;
    @SerializedName("procRefusalAmount")
    @Expose
    private Integer procRefusalAmount;
    @SerializedName("procRequestAmount")
    @Expose
    private Integer procRequestAmount;
    @SerializedName("provDateTime")
    @Expose
    private Object provDateTime;
    @SerializedName("provisionExplanation")
    @Expose
    private Object provisionExplanation;
    @SerializedName("provisionTotal")
    @Expose
    private Integer provisionTotal;
    @SerializedName("refusalAmount")
    @Expose
    private Integer refusalAmount;
    @SerializedName("reqCureDayCount")
    @Expose
    private Integer reqCureDayCount;
    @SerializedName("requestAmount")
    @Expose
    private Integer requestAmount;
    @SerializedName("sgkAmount")
    @Expose
    private Integer sgkAmount;
    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("subPackageDate")
    @Expose
    private Object subPackageDate;
    @SerializedName("subPackageId")
    @Expose
    private Object subPackageId;
    @SerializedName("swiftCode")
    @Expose
    private String swiftCode;
    @SerializedName("sysRequestAmount")
    @Expose
    private Integer sysRequestAmount;
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("userId")
    @Expose
    private Object userId;
    @SerializedName("parity")
    @Expose
    private Integer parity;
    @SerializedName("insuredAmount")
    @Expose
    private Integer insuredAmount;
    @SerializedName("netProvisionAmount")
    @Expose
    private Integer netProvisionAmount;
    @SerializedName("fdaySeance")
    @Expose
    private Object fdaySeance;
    @SerializedName("rcoverPrice")
    @Expose
    private Integer rcoverPrice;

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }

    public Integer getSfNo() {
        return sfNo;
    }

    public void setSfNo(Integer sfNo) {
        this.sfNo = sfNo;
    }

    public Integer getAddOrderNo() {
        return addOrderNo;
    }

    public void setAddOrderNo(Integer addOrderNo) {
        this.addOrderNo = addOrderNo;
    }

    public Object getExtReference() {
        return extReference;
    }

    public void setExtReference(Object extReference) {
        this.extReference = extReference;
    }

    public Integer getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(Integer locationCode) {
        this.locationCode = locationCode;
    }

    public String getCoverCode() {
        return coverCode;
    }

    public void setCoverCode(String coverCode) {
        this.coverCode = coverCode;
    }

    public String getCoverCodeExplanation() {
        return coverCodeExplanation;
    }

    public void setCoverCodeExplanation(String coverCodeExplanation) {
        this.coverCodeExplanation = coverCodeExplanation;
    }

    public Object getAddProcAmount() {
        return addProcAmount;
    }

    public void setAddProcAmount(Object addProcAmount) {
        this.addProcAmount = addProcAmount;
    }

    public Object getBreResultCode() {
        return breResultCode;
    }

    public void setBreResultCode(Object breResultCode) {
        this.breResultCode = breResultCode;
    }

    public Object getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Object countryCode) {
        this.countryCode = countryCode;
    }

    public Object getCoverDistributionAmount() {
        return coverDistributionAmount;
    }

    public void setCoverDistributionAmount(Object coverDistributionAmount) {
        this.coverDistributionAmount = coverDistributionAmount;
    }

    public Integer getDaySeance() {
        return daySeance;
    }

    public void setDaySeance(Integer daySeance) {
        this.daySeance = daySeance;
    }

    public Object getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(Object doctorCode) {
        this.doctorCode = doctorCode;
    }

    public Object getDoctorStatus() {
        return doctorStatus;
    }

    public void setDoctorStatus(Object doctorStatus) {
        this.doctorStatus = doctorStatus;
    }

    public Object getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Object entryDate) {
        this.entryDate = entryDate;
    }

    public Integer getExemptionAmount() {
        return exemptionAmount;
    }

    public void setExemptionAmount(Integer exemptionAmount) {
        this.exemptionAmount = exemptionAmount;
    }

    public Double getExemptionRate() {
        return exemptionRate;
    }

    public void setExemptionRate(Double exemptionRate) {
        this.exemptionRate = exemptionRate;
    }

    public Object getInstAddProcAmount() {
        return instAddProcAmount;
    }

    public void setInstAddProcAmount(Object instAddProcAmount) {
        this.instAddProcAmount = instAddProcAmount;
    }

    public Integer getInstExemptionAmount() {
        return instExemptionAmount;
    }

    public void setInstExemptionAmount(Integer instExemptionAmount) {
        this.instExemptionAmount = instExemptionAmount;
    }

    public Integer getInstRequestAmount() {
        return instRequestAmount;
    }

    public void setInstRequestAmount(Integer instRequestAmount) {
        this.instRequestAmount = instRequestAmount;
    }

    public Integer getInstWrongAmount() {
        return instWrongAmount;
    }

    public void setInstWrongAmount(Integer instWrongAmount) {
        this.instWrongAmount = instWrongAmount;
    }

    public Object getIsExGratia() {
        return isExGratia;
    }

    public void setIsExGratia(Object isExGratia) {
        this.isExGratia = isExGratia;
    }

    public Integer getIsPoolCover() {
        return isPoolCover;
    }

    public void setIsPoolCover(Integer isPoolCover) {
        this.isPoolCover = isPoolCover;
    }

    public Integer getIsSpecialCover() {
        return isSpecialCover;
    }

    public void setIsSpecialCover(Integer isSpecialCover) {
        this.isSpecialCover = isSpecialCover;
    }

    public Object getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Object orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getProcRefusalAmount() {
        return procRefusalAmount;
    }

    public void setProcRefusalAmount(Integer procRefusalAmount) {
        this.procRefusalAmount = procRefusalAmount;
    }

    public Integer getProcRequestAmount() {
        return procRequestAmount;
    }

    public void setProcRequestAmount(Integer procRequestAmount) {
        this.procRequestAmount = procRequestAmount;
    }

    public Object getProvDateTime() {
        return provDateTime;
    }

    public void setProvDateTime(Object provDateTime) {
        this.provDateTime = provDateTime;
    }

    public Object getProvisionExplanation() {
        return provisionExplanation;
    }

    public void setProvisionExplanation(Object provisionExplanation) {
        this.provisionExplanation = provisionExplanation;
    }

    public Integer getProvisionTotal() {
        return provisionTotal;
    }

    public void setProvisionTotal(Integer provisionTotal) {
        this.provisionTotal = provisionTotal;
    }

    public Integer getRefusalAmount() {
        return refusalAmount;
    }

    public void setRefusalAmount(Integer refusalAmount) {
        this.refusalAmount = refusalAmount;
    }

    public Integer getReqCureDayCount() {
        return reqCureDayCount;
    }

    public void setReqCureDayCount(Integer reqCureDayCount) {
        this.reqCureDayCount = reqCureDayCount;
    }

    public Integer getRequestAmount() {
        return requestAmount;
    }

    public void setRequestAmount(Integer requestAmount) {
        this.requestAmount = requestAmount;
    }

    public Integer getSgkAmount() {
        return sgkAmount;
    }

    public void setSgkAmount(Integer sgkAmount) {
        this.sgkAmount = sgkAmount;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Object getSubPackageDate() {
        return subPackageDate;
    }

    public void setSubPackageDate(Object subPackageDate) {
        this.subPackageDate = subPackageDate;
    }

    public Object getSubPackageId() {
        return subPackageId;
    }

    public void setSubPackageId(Object subPackageId) {
        this.subPackageId = subPackageId;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    public Integer getSysRequestAmount() {
        return sysRequestAmount;
    }

    public void setSysRequestAmount(Integer sysRequestAmount) {
        this.sysRequestAmount = sysRequestAmount;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Object getUserId() {
        return userId;
    }

    public void setUserId(Object userId) {
        this.userId = userId;
    }

    public Integer getParity() {
        return parity;
    }

    public void setParity(Integer parity) {
        this.parity = parity;
    }

    public Integer getInsuredAmount() {
        return insuredAmount;
    }

    public void setInsuredAmount(Integer insuredAmount) {
        this.insuredAmount = insuredAmount;
    }

    public Integer getNetProvisionAmount() {
        return netProvisionAmount;
    }

    public void setNetProvisionAmount(Integer netProvisionAmount) {
        this.netProvisionAmount = netProvisionAmount;
    }

    public Object getFdaySeance() {
        return fdaySeance;
    }

    public void setFdaySeance(Object fdaySeance) {
        this.fdaySeance = fdaySeance;
    }

    public Integer getRcoverPrice() {
        return rcoverPrice;
    }

    public void setRcoverPrice(Integer rcoverPrice) {
        this.rcoverPrice = rcoverPrice;
    }

}
