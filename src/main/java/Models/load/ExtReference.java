
package Models.load;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import test.IreadJsonFiles;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class ExtReference implements IreadJsonFiles {

    String path;
    Gson gson;
    Reader reader;

    public ExtReference(String path) {
        this.path = path;
    }

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("uiMessages")
    @Expose
    private Object uiMessages;
    @SerializedName("tracingId")
    @Expose
    private Object tracingId;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Object getUiMessages() {
        return uiMessages;
    }

    public void setUiMessages(Object uiMessages) {
        this.uiMessages = uiMessages;
    }

    public Object getTracingId() {
        return tracingId;
    }

    public void setTracingId(Object tracingId) {
        this.tracingId = tracingId;
    }

    @Override
    public void readJson() {

        gson = new Gson();

        try {
            reader = new FileReader(System.getProperty("user.dir") + path);
            // Convert JSON File to Java Object
            ExtReference staff = gson.fromJson(reader, ExtReference.class);

            // print staff
            String policyRef = staff.getData().getHealthClaimDetailDto().getClaimPolicyBasesDto().getPolicyRef();
            Integer oarNo = staff.getData().getHealthClaimDetailDto().getClaimPolOarDto().getOarNo();
            System.out.println("PolicyRef : " + policyRef);
            System.out.println("OarNo : " + oarNo);

            System.out.println(policyRef + "-" + oarNo);

            System.out.println("Contract Id : " + staff.getData().getHealthClaimDetailDto().getClaimPolOarDto().getContractId());
            System.out.println("Institude Code : " + staff.getData().getHealthClaimDetailDto().getInstituteCode());
            System.out.println("Contract Id : " + staff.getData().getHealthClaimDetailDto().getClaimPolicyBasesDto().getContractId());


        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
