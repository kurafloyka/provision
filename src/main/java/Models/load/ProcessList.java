
package Models.load;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProcessList {

    @SerializedName("claimId")
    @Expose
    private Integer claimId;
    @SerializedName("sfNo")
    @Expose
    private Integer sfNo;
    @SerializedName("addOrderNo")
    @Expose
    private Integer addOrderNo;
    @SerializedName("extReference")
    @Expose
    private Object extReference;
    @SerializedName("seqNo")
    @Expose
    private Integer seqNo;
    @SerializedName("locationCode")
    @Expose
    private Integer locationCode;
    @SerializedName("coverCode")
    @Expose
    private String coverCode;
    @SerializedName("groupNo")
    @Expose
    private Integer groupNo;
    @SerializedName("processCodeMain")
    @Expose
    private Integer processCodeMain;
    @SerializedName("processCodeSub1")
    @Expose
    private Integer processCodeSub1;
    @SerializedName("processCodeSub2")
    @Expose
    private Integer processCodeSub2;
    @SerializedName("breResultCode")
    @Expose
    private Object breResultCode;
    @SerializedName("diagnosisId")
    @Expose
    private Object diagnosisId;
    @SerializedName("discountGroupCode")
    @Expose
    private String discountGroupCode;
    @SerializedName("doctorCode")
    @Expose
    private Object doctorCode;
    @SerializedName("doctorStatus")
    @Expose
    private Object doctorStatus;
    @SerializedName("drgCode")
    @Expose
    private Object drgCode;
    @SerializedName("entranceDate")
    @Expose
    private Object entranceDate;
    @SerializedName("explanation")
    @Expose
    private Object explanation;
    @SerializedName("indemnityAmount")
    @Expose
    private Integer indemnityAmount;
    @SerializedName("instIndemnityAmount")
    @Expose
    private Integer instIndemnityAmount;
    @SerializedName("orderNo")
    @Expose
    private Object orderNo;
    @SerializedName("physiotherapySession")
    @Expose
    private Object physiotherapySession;
    @SerializedName("procType")
    @Expose
    private Object procType;
    @SerializedName("processCount")
    @Expose
    private Integer processCount;
    @SerializedName("processDate")
    @Expose
    private Object processDate;
    @SerializedName("processGroup")
    @Expose
    private Object processGroup;
    @SerializedName("relatedProcess")
    @Expose
    private Object relatedProcess;
    @SerializedName("relatedProcessListType")
    @Expose
    private Object relatedProcessListType;
    @SerializedName("reqProcessCode")
    @Expose
    private Object reqProcessCode;
    @SerializedName("reqProcessListType")
    @Expose
    private Object reqProcessListType;
    @SerializedName("reqProcessName")
    @Expose
    private String reqProcessName;
    @SerializedName("rightLeft")
    @Expose
    private Object rightLeft;
    @SerializedName("sgkAmount")
    @Expose
    private Object sgkAmount;
    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("surgeryId")
    @Expose
    private Object surgeryId;
    @SerializedName("swiftCode")
    @Expose
    private Object swiftCode;
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("userid")
    @Expose
    private Object userid;
    @SerializedName("vatRate")
    @Expose
    private Object vatRate;
    @SerializedName("claimSurgeryDetailDto")
    @Expose
    private Object claimSurgeryDetailDto;
    @SerializedName("healthClaimDiagnosisDto")
    @Expose
    private Object healthClaimDiagnosisDto;
    @SerializedName("notMatch")
    @Expose
    private Boolean notMatch;
    @SerializedName("consumableProcess")
    @Expose
    private Boolean consumableProcess;

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }

    public Integer getSfNo() {
        return sfNo;
    }

    public void setSfNo(Integer sfNo) {
        this.sfNo = sfNo;
    }

    public Integer getAddOrderNo() {
        return addOrderNo;
    }

    public void setAddOrderNo(Integer addOrderNo) {
        this.addOrderNo = addOrderNo;
    }

    public Object getExtReference() {
        return extReference;
    }

    public void setExtReference(Object extReference) {
        this.extReference = extReference;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    public Integer getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(Integer locationCode) {
        this.locationCode = locationCode;
    }

    public String getCoverCode() {
        return coverCode;
    }

    public void setCoverCode(String coverCode) {
        this.coverCode = coverCode;
    }

    public Integer getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(Integer groupNo) {
        this.groupNo = groupNo;
    }

    public Integer getProcessCodeMain() {
        return processCodeMain;
    }

    public void setProcessCodeMain(Integer processCodeMain) {
        this.processCodeMain = processCodeMain;
    }

    public Integer getProcessCodeSub1() {
        return processCodeSub1;
    }

    public void setProcessCodeSub1(Integer processCodeSub1) {
        this.processCodeSub1 = processCodeSub1;
    }

    public Integer getProcessCodeSub2() {
        return processCodeSub2;
    }

    public void setProcessCodeSub2(Integer processCodeSub2) {
        this.processCodeSub2 = processCodeSub2;
    }

    public Object getBreResultCode() {
        return breResultCode;
    }

    public void setBreResultCode(Object breResultCode) {
        this.breResultCode = breResultCode;
    }

    public Object getDiagnosisId() {
        return diagnosisId;
    }

    public void setDiagnosisId(Object diagnosisId) {
        this.diagnosisId = diagnosisId;
    }

    public String getDiscountGroupCode() {
        return discountGroupCode;
    }

    public void setDiscountGroupCode(String discountGroupCode) {
        this.discountGroupCode = discountGroupCode;
    }

    public Object getDoctorCode() {
        return doctorCode;
    }

    public void setDoctorCode(Object doctorCode) {
        this.doctorCode = doctorCode;
    }

    public Object getDoctorStatus() {
        return doctorStatus;
    }

    public void setDoctorStatus(Object doctorStatus) {
        this.doctorStatus = doctorStatus;
    }

    public Object getDrgCode() {
        return drgCode;
    }

    public void setDrgCode(Object drgCode) {
        this.drgCode = drgCode;
    }

    public Object getEntranceDate() {
        return entranceDate;
    }

    public void setEntranceDate(Object entranceDate) {
        this.entranceDate = entranceDate;
    }

    public Object getExplanation() {
        return explanation;
    }

    public void setExplanation(Object explanation) {
        this.explanation = explanation;
    }

    public Integer getIndemnityAmount() {
        return indemnityAmount;
    }

    public void setIndemnityAmount(Integer indemnityAmount) {
        this.indemnityAmount = indemnityAmount;
    }

    public Integer getInstIndemnityAmount() {
        return instIndemnityAmount;
    }

    public void setInstIndemnityAmount(Integer instIndemnityAmount) {
        this.instIndemnityAmount = instIndemnityAmount;
    }

    public Object getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Object orderNo) {
        this.orderNo = orderNo;
    }

    public Object getPhysiotherapySession() {
        return physiotherapySession;
    }

    public void setPhysiotherapySession(Object physiotherapySession) {
        this.physiotherapySession = physiotherapySession;
    }

    public Object getProcType() {
        return procType;
    }

    public void setProcType(Object procType) {
        this.procType = procType;
    }

    public Integer getProcessCount() {
        return processCount;
    }

    public void setProcessCount(Integer processCount) {
        this.processCount = processCount;
    }

    public Object getProcessDate() {
        return processDate;
    }

    public void setProcessDate(Object processDate) {
        this.processDate = processDate;
    }

    public Object getProcessGroup() {
        return processGroup;
    }

    public void setProcessGroup(Object processGroup) {
        this.processGroup = processGroup;
    }

    public Object getRelatedProcess() {
        return relatedProcess;
    }

    public void setRelatedProcess(Object relatedProcess) {
        this.relatedProcess = relatedProcess;
    }

    public Object getRelatedProcessListType() {
        return relatedProcessListType;
    }

    public void setRelatedProcessListType(Object relatedProcessListType) {
        this.relatedProcessListType = relatedProcessListType;
    }

    public Object getReqProcessCode() {
        return reqProcessCode;
    }

    public void setReqProcessCode(Object reqProcessCode) {
        this.reqProcessCode = reqProcessCode;
    }

    public Object getReqProcessListType() {
        return reqProcessListType;
    }

    public void setReqProcessListType(Object reqProcessListType) {
        this.reqProcessListType = reqProcessListType;
    }

    public String getReqProcessName() {
        return reqProcessName;
    }

    public void setReqProcessName(String reqProcessName) {
        this.reqProcessName = reqProcessName;
    }

    public Object getRightLeft() {
        return rightLeft;
    }

    public void setRightLeft(Object rightLeft) {
        this.rightLeft = rightLeft;
    }

    public Object getSgkAmount() {
        return sgkAmount;
    }

    public void setSgkAmount(Object sgkAmount) {
        this.sgkAmount = sgkAmount;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Object getSurgeryId() {
        return surgeryId;
    }

    public void setSurgeryId(Object surgeryId) {
        this.surgeryId = surgeryId;
    }

    public Object getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(Object swiftCode) {
        this.swiftCode = swiftCode;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Object getUserid() {
        return userid;
    }

    public void setUserid(Object userid) {
        this.userid = userid;
    }

    public Object getVatRate() {
        return vatRate;
    }

    public void setVatRate(Object vatRate) {
        this.vatRate = vatRate;
    }

    public Object getClaimSurgeryDetailDto() {
        return claimSurgeryDetailDto;
    }

    public void setClaimSurgeryDetailDto(Object claimSurgeryDetailDto) {
        this.claimSurgeryDetailDto = claimSurgeryDetailDto;
    }

    public Object getHealthClaimDiagnosisDto() {
        return healthClaimDiagnosisDto;
    }

    public void setHealthClaimDiagnosisDto(Object healthClaimDiagnosisDto) {
        this.healthClaimDiagnosisDto = healthClaimDiagnosisDto;
    }

    public Boolean getNotMatch() {
        return notMatch;
    }

    public void setNotMatch(Boolean notMatch) {
        this.notMatch = notMatch;
    }

    public Boolean getConsumableProcess() {
        return consumableProcess;
    }

    public void setConsumableProcess(Boolean consumableProcess) {
        this.consumableProcess = consumableProcess;
    }

}
