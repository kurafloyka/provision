
package Models.load;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClaimPolOarDto {

    @SerializedName("claimId")
    @Expose
    private Integer claimId;
    @SerializedName("sfNo")
    @Expose
    private Object sfNo;
    @SerializedName("addOrderNo")
    @Expose
    private Object addOrderNo;
    @SerializedName("extReference")
    @Expose
    private Object extReference;
    @SerializedName("contractId")
    @Expose
    private Integer contractId;
    @SerializedName("oarNo")
    @Expose
    private Integer oarNo;
    @SerializedName("objectId")
    @Expose
    private Object objectId;
    @SerializedName("versionNo")
    @Expose
    private Integer versionNo;

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }

    public Object getSfNo() {
        return sfNo;
    }

    public void setSfNo(Object sfNo) {
        this.sfNo = sfNo;
    }

    public Object getAddOrderNo() {
        return addOrderNo;
    }

    public void setAddOrderNo(Object addOrderNo) {
        this.addOrderNo = addOrderNo;
    }

    public Object getExtReference() {
        return extReference;
    }

    public void setExtReference(Object extReference) {
        this.extReference = extReference;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Integer getOarNo() {
        return oarNo;
    }

    public void setOarNo(Integer oarNo) {
        this.oarNo = oarNo;
    }

    public Object getObjectId() {
        return objectId;
    }

    public void setObjectId(Object objectId) {
        this.objectId = objectId;
    }

    public Integer getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(Integer versionNo) {
        this.versionNo = versionNo;
    }

}
