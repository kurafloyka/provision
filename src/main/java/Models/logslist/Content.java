
package Models.logslist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Content {

    @SerializedName("logId")
    @Expose
    private Integer logId;
    @SerializedName("orderNo")
    @Expose
    private Integer orderNo;
    @SerializedName("serviceName")
    @Expose
    private String serviceName;
    @SerializedName("processInfo")
    @Expose
    private String processInfo;
    @SerializedName("instituteCode")
    @Expose
    private String instituteCode;
    @SerializedName("insuredNo")
    @Expose
    private Object insuredNo;
    @SerializedName("insuredNoType")
    @Expose
    private Object insuredNoType;
    @SerializedName("policyRef")
    @Expose
    private Object policyRef;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("logDate")
    @Expose
    private String logDate;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("logMode")
    @Expose
    private String logMode;
    @SerializedName("logType")
    @Expose
    private Object logType;
    @SerializedName("logSource")
    @Expose
    private String logSource;

    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    public Integer getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getProcessInfo() {
        return processInfo;
    }

    public void setProcessInfo(String processInfo) {
        this.processInfo = processInfo;
    }

    public String getInstituteCode() {
        return instituteCode;
    }

    public void setInstituteCode(String instituteCode) {
        this.instituteCode = instituteCode;
    }

    public Object getInsuredNo() {
        return insuredNo;
    }

    public void setInsuredNo(Object insuredNo) {
        this.insuredNo = insuredNo;
    }

    public Object getInsuredNoType() {
        return insuredNoType;
    }

    public void setInsuredNoType(Object insuredNoType) {
        this.insuredNoType = insuredNoType;
    }

    public Object getPolicyRef() {
        return policyRef;
    }

    public void setPolicyRef(Object policyRef) {
        this.policyRef = policyRef;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLogDate() {
        return logDate;
    }

    public void setLogDate(String logDate) {
        this.logDate = logDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLogMode() {
        return logMode;
    }

    public void setLogMode(String logMode) {
        this.logMode = logMode;
    }

    public Object getLogType() {
        return logType;
    }

    public void setLogType(Object logType) {
        this.logType = logType;
    }

    public String getLogSource() {
        return logSource;
    }

    public void setLogSource(String logSource) {
        this.logSource = logSource;
    }

}
