
package Models.logslist;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import test.IreadJsonFiles;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

public class LogList implements IreadJsonFiles {

    String path;
    Gson gson;
    Reader reader;

    public LogList(String path) {
        this.path = path;
    }

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("uiMessages")
    @Expose
    private Object uiMessages;
    @SerializedName("tracingId")
    @Expose
    private Object tracingId;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Object getUiMessages() {
        return uiMessages;
    }

    public void setUiMessages(Object uiMessages) {
        this.uiMessages = uiMessages;
    }

    public Object getTracingId() {
        return tracingId;
    }

    public void setTracingId(Object tracingId) {
        this.tracingId = tracingId;
    }

    @Override
    public void readJson() {

        gson = new Gson();

        try {
            reader = new FileReader(System.getProperty("user.dir") + path);
            // Convert JSON File to Java Object
            LogList staff = gson.fromJson(reader, LogList.class);

            // print staff
            List<Content> contentList = staff.getData().getContent();

            for (Content content : contentList) {

                System.out.println("Insured No : " + content.getInsuredNo());
                System.out.println("Institude Code : " + content.getInstituteCode());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
