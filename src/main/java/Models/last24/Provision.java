package Models.last24;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Provision {

    @SerializedName("claimId")
    @Expose
    private Integer claimId;
    @SerializedName("sfNo")
    @Expose
    private Integer sfNo;
    @SerializedName("addOrderNo")
    @Expose
    private Integer addOrderNo;
    @SerializedName("versionNo")
    @Expose
    private Integer versionNo;
    @SerializedName("approvedStatus")
    @Expose
    private String approvedStatus;
    @SerializedName("breResultCode")
    @Expose
    private Object breResultCode;
    @SerializedName("contractId")
    @Expose
    private Integer contractId;
    @SerializedName("customerType")
    @Expose
    private Object customerType;
    @SerializedName("entryDate")
    @Expose
    private String entryDate;
    @SerializedName("groupCode")
    @Expose
    private Object groupCode;
    @SerializedName("hclmChannel")
    @Expose
    private String hclmChannel;
    @SerializedName("hclmUsage")
    @Expose
    private Object hclmUsage;
    @SerializedName("instituteCode")
    @Expose
    private Integer instituteCode;
    @SerializedName("isLimitUpdated")
    @Expose
    private Boolean isLimitUpdated;
    @SerializedName("logId")
    @Expose
    private Integer logId;
    @SerializedName("partitionNo")
    @Expose
    private Integer partitionNo;
    @SerializedName("partnerId")
    @Expose
    private Integer partnerId;
    @SerializedName("complementaryTypeCode")
    @Expose
    private Object complementaryTypeCode;
    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("tpaCompanyCode")
    @Expose
    private Object tpaCompanyCode;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("extReference")
    @Expose
    private String extReference;

    public Integer getClaimId() {
        return claimId;
    }

    public void setClaimId(Integer claimId) {
        this.claimId = claimId;
    }

    public Integer getSfNo() {
        return sfNo;
    }

    public void setSfNo(Integer sfNo) {
        this.sfNo = sfNo;
    }

    public Integer getAddOrderNo() {
        return addOrderNo;
    }

    public void setAddOrderNo(Integer addOrderNo) {
        this.addOrderNo = addOrderNo;
    }

    public Integer getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(Integer versionNo) {
        this.versionNo = versionNo;
    }

    public String getApprovedStatus() {
        return approvedStatus;
    }

    public void setApprovedStatus(String approvedStatus) {
        this.approvedStatus = approvedStatus;
    }

    public Object getBreResultCode() {
        return breResultCode;
    }

    public void setBreResultCode(Object breResultCode) {
        this.breResultCode = breResultCode;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Object getCustomerType() {
        return customerType;
    }

    public void setCustomerType(Object customerType) {
        this.customerType = customerType;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public Object getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(Object groupCode) {
        this.groupCode = groupCode;
    }

    public String getHclmChannel() {
        return hclmChannel;
    }

    public void setHclmChannel(String hclmChannel) {
        this.hclmChannel = hclmChannel;
    }

    public Object getHclmUsage() {
        return hclmUsage;
    }

    public void setHclmUsage(Object hclmUsage) {
        this.hclmUsage = hclmUsage;
    }

    public Integer getInstituteCode() {
        return instituteCode;
    }

    public void setInstituteCode(Integer instituteCode) {
        this.instituteCode = instituteCode;
    }

    public Boolean getIsLimitUpdated() {
        return isLimitUpdated;
    }

    public void setIsLimitUpdated(Boolean isLimitUpdated) {
        this.isLimitUpdated = isLimitUpdated;
    }

    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    public Integer getPartitionNo() {
        return partitionNo;
    }

    public void setPartitionNo(Integer partitionNo) {
        this.partitionNo = partitionNo;
    }

    public Integer getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }

    public Object getComplementaryTypeCode() {
        return complementaryTypeCode;
    }

    public void setComplementaryTypeCode(Object complementaryTypeCode) {
        this.complementaryTypeCode = complementaryTypeCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Object getTpaCompanyCode() {
        return tpaCompanyCode;
    }

    public void setTpaCompanyCode(Object tpaCompanyCode) {
        this.tpaCompanyCode = tpaCompanyCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getExtReference() {
        return extReference;
    }

    public void setExtReference(String extReference) {
        this.extReference = extReference;
    }

    @Override
    public String toString() {
        return "Provision{" +
                "claimId=" + claimId +
                ", sfNo=" + sfNo +
                ", addOrderNo=" + addOrderNo +
                ", versionNo=" + versionNo +
                ", approvedStatus='" + approvedStatus + '\'' +
                ", breResultCode=" + breResultCode +
                ", contractId=" + contractId +
                ", customerType=" + customerType +
                ", entryDate='" + entryDate + '\'' +
                ", groupCode=" + groupCode +
                ", hclmChannel='" + hclmChannel + '\'' +
                ", hclmUsage=" + hclmUsage +
                ", instituteCode=" + instituteCode +
                ", isLimitUpdated=" + isLimitUpdated +
                ", logId=" + logId +
                ", partitionNo=" + partitionNo +
                ", partnerId=" + partnerId +
                ", complementaryTypeCode=" + complementaryTypeCode +
                ", statusCode='" + statusCode + '\'' +
                ", tpaCompanyCode=" + tpaCompanyCode +
                ", userId='" + userId + '\'' +
                ", extReference='" + extReference + '\'' +
                '}';
    }
}
