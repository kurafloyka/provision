package Models.last24;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import test.IreadJsonFiles;

public class Last24 implements IreadJsonFiles {
    String path;
    Gson gson;
    Reader reader;

    public Last24(String path) {
        this.path = path;
    }
    @SerializedName("data")
    @Expose
    private List<Provision> data = null;
    @SerializedName("uiMessages")
    @Expose
    private Object uiMessages;
    @SerializedName("tracingId")
    @Expose
    private Object tracingId;

    public List<Provision> getData() {
        return data;
    }

    public void setData(List<Provision> data) {
        this.data = data;
    }

    public Object getUiMessages() {
        return uiMessages;
    }

    public void setUiMessages(Object uiMessages) {
        this.uiMessages = uiMessages;
    }

    public Object getTracingId() {
        return tracingId;
    }

    public void setTracingId(Object tracingId) {
        this.tracingId = tracingId;
    }

    @Override
    public void readJson() {


        gson = new Gson();

        try {
            reader = new FileReader(System.getProperty("user.dir") + path);
            // Convert JSON File to Java Object
            Last24 staff = gson.fromJson(reader, Last24.class);

            // print staff

            for (Provision provision : staff.getData()) {

                System.out.println("ExtReferenceId : " + provision.getExtReference());
                System.out.println("LogId : " + provision.getLogId());


            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
